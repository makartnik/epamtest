﻿using System;
using System.Text;

namespace CSV.Generation.Demo
{
    /// <summary>
    /// Параметры приложения.
    /// </summary>
    public class Options
    {
        /// <summary>
        /// Количество колонок.
        /// </summary>
        private int _columnCount;

        /// <summary>
        /// Количесвво колонок.
        /// </summary>
        public int ColumnCount
        {
            get => _columnCount;
            set
            {
                if (value < 0)
                    throw new ArgumentException("Неверное значение количества столбцов.", nameof(value));
                _columnCount = value;
            }
        }

        /// <summary>
        /// Количесвто строк.
        /// </summary>
        private int _rowCount;

        /// <summary>
        /// Количество строк.
        /// </summary>
        public int RowCount
        {
            get => _rowCount;
            set
            {
                if (value < 0)
                    throw new ArgumentException("Неверное значение количества строк.", nameof(value));
                _rowCount = value;
            }
        }

        /// <summary>
        /// Путь выходного фафла.
        /// </summary>
        public string Output { get; set; }

        /// <summary>
        /// Кодировка файла.
        /// </summary>
        public Encoding Encoding { get; set; }

        /// <summary>
        /// Максимальная длина строк.
        /// </summary>
        private int _maxLength;

        /// <summary>
        /// Максимальная длина строк.
        /// </summary>
        public int MaxLength
        {
            get => _maxLength;
            set
            {
                if (value < 1)
                    throw new ArgumentException("Неверное значение максимальной длины строки.", nameof(value));
                _maxLength = value;
            }
        }
    }
}