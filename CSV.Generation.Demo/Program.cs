﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSV.Generation.Demo
{
    class Program
    {
        /// <summary>
        /// Точка входа.
        /// </summary>
        /// <param name="args">Параметры командной строки.</param>
        static void Main(string[] args)
        {
            try
            {
                var options = GetOptions(args);
                var random = new Random();
                var generator = new Generator(random);
                generator.Generate(options.Output, options.Encoding, options.RowCount, options.ColumnCount,
                    options.MaxLength);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Получает параметры приложения из аргументов командной строки.
        /// </summary>
        /// <param name="args">Аргументы командной строки.</param>
        /// <returns>Возвращает параметрыт приложение.</returns>
        private static Options GetOptions(string[] args)
        {
            if (args.Length != 10)
                throw new ArgumentException("Неверное количество параметров.", nameof(args));
            var arguments = new Dictionary<string, string>();
            for (int i = 0; i < args.Length; i += 2)
                arguments.Add(args[i], args[i + 1]);
            try
            {
                var options = new Options
                {
                    ColumnCount = int.Parse(arguments["-col"]),
                    RowCount = int.Parse(arguments["-row"]),
                    Output = arguments["-out"],
                    Encoding = Encoding.GetEncoding(arguments["-enc"]),
                    MaxLength = int.Parse(arguments["-len"])
                };
                return options;
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentException("Указаны не все параметры.");
            }
        }
    }
}