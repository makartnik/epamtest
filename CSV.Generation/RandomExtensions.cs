﻿using System;

namespace CSV.Generation
{
    /// <summary>
    /// Расширение для Random
    /// </summary>
    public static class RandomExtensions
    {
        /// <summary>
        /// Генерирует случайный символ.
        /// </summary>
        /// <param name="random">Экземпляр Random</param>
        /// <returns>Возвращает сгенерированный символ.</returns>
        public static char NextChar(this Random random)
        {
            return (char) random.Next(char.MinValue, char.MaxValue + 1);
        }
    }
}