﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSV.Generation
{
    /// <summary>
    /// Генератор CSV-файла.
    /// </summary>
    public class Generator
    {
        /// <summary>
        /// Генератор псевдослучайных чисел.
        /// </summary>
        private readonly Random _random;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="random">Генератор псевдослучайных чисел.</param>
        public Generator(Random random)
        {
            _random = random;
        }

        /// <summary>
        /// Генерирует CSV-файл.
        /// </summary>
        /// <param name="outPath">Путь файла.</param>
        /// <param name="encoding">Кодировка файла.</param>
        /// <param name="rowCount">Количество строк CSV-файла.</param>
        /// <param name="columnCount">Количесвто колонок CSV-файла.</param>
        /// <param name="maxLength">Максимальная длина строк</param>
        public void Generate(string outPath, Encoding encoding, int rowCount, int columnCount, int maxLength)
        {
            var headline = GenerateHeadline(columnCount, maxLength).ToList();
            try
            {
                using (var writer = new Writer(outPath, encoding, headline))
                {
                    for (int i = 0; i < rowCount; ++i)
                    {
                        var row = GenerateRow(headline, maxLength).ToList();
                        writer.WriteRow(row);
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
                throw new UnauthorizedAccessException("Отказано в доступе.");
            }
        }

        /// <summary>
        /// Генерирует заголовок CSV-файла.
        /// </summary>
        /// <param name="columnCount">Количество колонок.</param>
        /// <param name="maxLength">Максимальная длина строк.</param>
        /// <returns>Возвращает сгенерированный заголовок.</returns>
        private IEnumerable<Column> GenerateHeadline(int columnCount, int maxLength)
        {
            var generator = new ColumnGenerator(_random);
            for (int i = 0; i < columnCount; ++i)
                yield return generator.Generate(maxLength);
        }

        /// <summary>
        /// Генерирует строку CSV-файла.
        /// </summary>
        /// <param name="headline">Заголовок.</param>
        /// <param name="maxLength">Максимальная длина строк.</param>
        /// <returns>Возвращает сгенерированную строку.</returns>
        private IEnumerable<Cell> GenerateRow(IEnumerable<Column> headline, int maxLength)
        {
            var generator = new CellGenerator(_random);
            for (int i = 0; i < headline.Count(); i++)
                yield return generator.Generate(headline.ElementAt(i).Type, maxLength);
        }
    }
}