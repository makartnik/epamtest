﻿using System;
using System.Linq;
using System.Text;

namespace CSV.Generation
{
    /// <summary>
    /// Генератор колонок.
    /// </summary>
    public class ColumnGenerator
    {
        /// <summary>
        /// Генератор псевдослучайных чисел.
        /// </summary>
        private readonly Random _random;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="random">Генератор псевдослучайных чисел.</param>
        public ColumnGenerator(Random random)
        {
            _random = random;
        }

        /// <summary>
        /// Генерирует колонку.
        /// </summary>
        /// <param name="maxLength">Максимальная длина строк.</param>
        /// <returns>Возвращает сгенерированную колонку.</returns>
        public Column Generate(int maxLength)
        {
            var type = GenerateType();
            var name = GenerateName(maxLength);
            return new Column
            {
                Name = name,
                Type = type
            };
        }

        /// <summary>
        /// Генерирует имя колонки.
        /// </summary>
        /// <param name="maxLength">Максимальная длина имени колонки.</param>
        /// <returns>Возвращает сгенерированное имя.</returns>
        private string GenerateName(int maxLength)
        {
            int length = _random.Next() % maxLength + 1;
            StringBuilder builder = new StringBuilder();
            while (builder.Length != length)
            {
                char c = _random.NextChar();
                if (c != ' ' && c != '\n' && c != '\r')
                    builder.Append(c);
            }
            return builder.ToString();
        }

        /// <summary>
        /// Генерирует тип колоки.
        /// </summary>
        /// <returns>Возвращает сгенерированный тип колонки.</returns>
        private ColumnType GenerateType()
        {
            var types = Enum.GetValues(typeof(ColumnType)).Cast<ColumnType>().ToList();
            int index = _random.Next(0, types.Count);
            return types[index];
        }
    }
}