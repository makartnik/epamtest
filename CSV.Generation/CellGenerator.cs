﻿using System;
using System.Text;

namespace CSV.Generation
{
    /// <summary>
    /// Генератор ячеек.
    /// </summary>
    public class CellGenerator
    {
        /// <summary>
        /// Генератор псевдослучайных чисел.
        /// </summary>
        private readonly Random _random;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="random">Генератор псевдослучайных чисел.</param>
        public CellGenerator(Random random)
        {
            _random = random;
        }

        /// <summary>
        /// Генерирует ячейку с указанным типом.
        /// </summary>
        /// <param name="type">Тип ячейки.</param>
        /// <param name="maxLength">Максимальная длина строк.</param>
        /// <returns>Возвращает сгенерированную ячейку.</returns>
        public Cell Generate(ColumnType type, int maxLength)
        {
            switch (type)
            {
                case ColumnType.Integer:
                    return GenerateIntegerCell();
                case ColumnType.Float:
                    return GenerateFloatCell();
                case ColumnType.Date:
                    return GenerateDateCell();
                case ColumnType.String:
                    return GenerateStringCell(maxLength);
                default:
                    throw new InvalidOperationException("Неверный формат ячейки.");
            }
        }

        /// <summary>
        /// Генерирует ячейку с целым числом.
        /// </summary>
        /// <returns>Возвращает сгенерированную ячейку.</returns>
        private Cell GenerateIntegerCell()
        {
            int value = _random.Next(int.MinValue, int.MaxValue);
            return new Cell {Value = value.ToString()};
        }

        /// <summary>
        /// Генерирует ячейку с числом с плавающей точкой.
        /// </summary>
        /// <returns>Возвращает сгенерированную ячейку.</returns>
        private Cell GenerateFloatCell()
        {
            double value = _random.Next(int.MinValue, int.MaxValue) * _random.NextDouble();
            return new Cell {Value = value.ToString("F")};
        }

        /// <summary>
        /// Генерирует ячейку с датой.
        /// </summary>
        /// <returns>Возвращает сгенерированную ячейку.</returns>
        private Cell GenerateDateCell()
        {
            int dayCount = (DateTime.MaxValue - DateTime.MinValue).Days;
            DateTime value = DateTime.MinValue.AddDays(_random.Next(dayCount));
            return new Cell {Value = value.ToShortDateString()};
        }

        /// <summary>
        /// Генерирует ячейку со строкой.
        /// </summary>
        /// <param name="maxLength">Максимальная длина строки.</param>
        /// <returns>Возвращает сгенерированную ячейку.</returns>
        private Cell GenerateStringCell(int maxLength)
        {
            int length = _random.Next() % maxLength + 1;
            bool requried = false;
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < length - 2; ++i)
            {
                char c = _random.NextChar();
                if (c == '\n' || c == ';')
                    requried = true;
                if (c == '"')
                {
                    if (i == length - 3)
                    {
                        --i;
                        continue;
                    }
                    ++i;
                    builder.Append('"');
                }
                builder.Append(c);
            }
            if (requried)
            {
                builder.Append('"');
                builder.Insert(0, '"');
            }
            else
            {
                while (builder.Length != length)
                {
                    char c = _random.NextChar();
                    if (c == '\n' || c == ';' || c == '"')
                        continue;
                    builder.Append(c);
                }
            }
            return new Cell {Value = builder.ToString()};
        }
    }
}