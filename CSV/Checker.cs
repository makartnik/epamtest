﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CSV
{
    /// <summary>
    /// Проверяет корректность ячейки.
    /// </summary>
    public class Checker
    {
        /// <summary>
        /// Проверят принадлежность ячейки указанному типу.
        /// </summary>
        /// <param name="cell">Проверяемая ячейка.</param>
        /// <param name="type">Тип колонки.</param>
        /// <returns>Возвращает true, если ячейка корректна, иначе false.</returns>
        public bool Check(Cell cell, ColumnType type)
        {
            switch (type)
            {
                case ColumnType.String:
                    return CheckString(cell);
                case ColumnType.Integer:
                    return CheckFloat(cell);
                case ColumnType.Float:
                    return CheckFloat(cell);
                case ColumnType.Date:
                    return CheckDate(cell);
                default:
                    throw new InvalidOperationException("Неверный тип ячейки.");
            }
        }

        /// <summary>
        /// Проверяет принадлежность ячейки строке.
        /// </summary>
        /// <param name="cell">Проверяемая ячейка.</param>
        /// <returns>Возвращает true, если ячейка корректна, иначе false.</returns>
        public bool CheckString(Cell cell)
        {
            var arr = cell.Value.ToCharArray();
            if (arr.Where(c => c == '"').Count() % 2 == 1)
                return false;
            List<int> quotesIndices = new List<int>();
            if (arr.Any(c => c == '\n' || c == ';'))
            {
                if (arr.First() != '"' || arr.Last() != '"')
                    return false;
                for (int i = 1; i < arr.Length - 1; ++i)
                    if (arr[i] == '"')
                        quotesIndices.Add(i);
            }
            else
            {
                for (int i = 0; i < arr.Length; ++i)
                    if (arr[i] == '"')
                        quotesIndices.Add(i);
            }
            if (quotesIndices.Count % 2 == 1)
                return false;
            for (int i = 0; i < quotesIndices.Count; i += 2)
                if (quotesIndices[i] + 1 != quotesIndices[i + 1])
                    return false;
            return true;
        }

        /// <summary>
        /// Проверяет принадлежность ячейки целому числу.
        /// </summary>
        /// <param name="cell">Проверяемая ячейка.</param>
        /// <returns>Возвращает true, если ячейка корректна, иначе false.</returns>
        public bool CheckInteger(Cell cell)
        {
            int tempInt;
            return int.TryParse(cell.Value, out tempInt);
        }

        /// <summary>
        /// Проверяет принадлежность ячейки числу с плавающей точкой.
        /// </summary>
        /// <param name="cell">Проверяемая ячейка.</param>
        /// <returns>Возвращает true, если ячейка корректна, иначе false.</returns>
        public bool CheckFloat(Cell cell)
        {
            double tempDouble;
            return double.TryParse(cell.Value, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign,
                NumberFormatInfo.CurrentInfo, out tempDouble);
        }

        /// <summary>
        /// Проверяет принадлежность ячейки дате.
        /// </summary>
        /// <param name="cell">Проверяемая ячейка.</param>
        /// <returns>Возвращает true, если ячейка корректна, иначе false.</returns>
        public bool CheckDate(Cell cell)
        {
            DateTime tempDate;
            return DateTime.TryParseExact(cell.Value, "dd.MM.yyyy",
                DateTimeFormatInfo.CurrentInfo, DateTimeStyles.None, out tempDate);
        }
    }
}