﻿namespace CSV
{
    /// <summary>
    /// Колонка таблицы.
    /// </summary>
    public class Column
    {
        /// <summary>
        /// Имя колонки.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Тип колонки.
        /// </summary>
        public ColumnType Type { get; set; }

        /// <summary>
        /// Преобразует колонку таблицы в строку.
        /// </summary>
        /// <returns>Возвращает строковое представление колонки.</returns>
        public override string ToString()
        {
            return string.Join(" ", Name, Type);
        }
    }
}