﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CSV
{
    /// <summary>
    /// Записывает CSV-файл.
    /// </summary>
    public class Writer: IDisposable
    {
        /// <summary>
        /// Писатель файла.
        /// </summary>
        private readonly StreamWriter _writer;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="path">Путь файла.</param>
        /// <param name="encoding">Кодировка файла.</param>
        /// <param name="headline">Заголовок CSV-файла.</param>
        public Writer(string path, Encoding encoding, IEnumerable<Column> headline)
        {
            _writer = new StreamWriter(path, false, encoding);
            WriteHeadline(headline);
        }

        /// <summary>
        /// Записывае заголовок CSV-файла.
        /// </summary>
        /// <param name="headline">Заголовок CSV-файла.</param>
        private void WriteHeadline(IEnumerable<Column> headline)
        {
            string line = string.Join("; ", headline);
            _writer.WriteLine(line);
        }

        /// <summary>
        /// Записывает строку CSV-файла.
        /// </summary>
        /// <param name="row">Строка таблицы.</param>
        public void WriteRow(IEnumerable<Cell> row)
        {
            string line = string.Join("; ", row);
            _writer.WriteLine(line);
        }

        /// <summary>
        /// Освобождает все ресурсы, используемые объектом StreamWriter.
        /// </summary>
        public void Dispose()
        {
            _writer.Dispose();
        }
    }
}