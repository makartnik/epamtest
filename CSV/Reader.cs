﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CSV
{
    /// <summary>
    /// Читает CSV-файл
    /// </summary>
    public class Reader : IDisposable
    {
        /// <summary>
        /// Читатель файла.
        /// </summary>
        private readonly StreamReader _reader;

        /// <summary>
        /// Парсер CSV-файла.
        /// </summary>
        private readonly Parser _parser;

        /// <summary>
        /// Заговоловок CSV-файла.
        /// </summary>
        public IEnumerable<Column> Headline { get; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="path">Путь файла.</param>
        /// <param name="encoding">Кодировка файла.</param>
        public Reader(string path, Encoding encoding)
        {
            _reader = new StreamReader(path, encoding);
            _parser = new Parser();
            Headline = ReadHeadline();
        }

        /// <summary>
        /// Чтение заголовка CSV вайла.
        /// </summary>
        /// <returns>Возвращает прочитанный заголовок.</returns>
        private IEnumerable<Column> ReadHeadline()
        {
            string line = _reader.ReadLine();
            IEnumerable<Column> headline;
            try
            {
                headline = _parser.ParseHeadline(line).ToList();
            }
            catch (FormatException)
            {
                throw new FileFormatException("Некорректный файл.");
            }
            if (headline.Select(c => c.Name).Distinct().Count() != headline.Count())
            {
                throw new FileFormatException("Некорректный файл.");
            }
            return headline;
        }

        /// <summary>
        /// Чтение строки файла.
        /// </summary>
        /// <returns>Возвращает прочитанную строку файла, если конец файла - возвраает null.</returns>
        private string ReadLine()
        {
            char last = (char) 0;
            var builder = new StringBuilder();
            while (true)
            {
                int read = _reader.Read();
                if (read == -1)
                    return (builder.Length > 0) ? builder.ToString() : null;
                var current = (char) read;
                if (current == '\n' && last == '\r')
                {
                    builder.Remove(builder.Length - 1, 1);
                    return builder.ToString();
                }
                builder.Append(current);
                last = current;
            }
        }

        /// <summary>
        /// Читает строку CSV-файла.
        /// </summary>
        /// <returns>Возвращает прочитанную строку или null, если конец файла.</returns>
        public IEnumerable<Cell> ReadRow()
        {
            string line = ReadLine();
            if (line == null)
                return null;
            while ((line.ToCharArray().Where(c => c == '"').Count()) % 2 == 1)
            {
                string s = ReadLine();
                if (s == null)
                    throw new FileFormatException("Некорректный файл.");
                line = string.Join("\r\n", line, s);
            }
            List<Cell> row;
            try
            {
                row = _parser.ParseRow(line).ToList();
            }
            catch (FormatException)
            {
                throw new FileFormatException("Некорректный файл.");
            }

            if (row.Count != Headline.Count())
                throw new FileFormatException("Некорректный файл.");
            var checker = new Checker();
            for (int i = 0; i < row.Count; ++i)
                if (!checker.Check(row[i], Headline.ElementAt(i).Type))
                    throw new FileFormatException("Некорректный файл.");
            return row;
        }

        /// <summary>
        /// Освобождает все ресурсы, используемые объектом StreamReader.
        /// </summary>
        public void Dispose()
        {
            _reader.Dispose();
        }
    }
}