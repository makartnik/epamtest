﻿using System;

namespace CSV
{
    /// <summary>
    /// Ячейка таблицы
    /// </summary>
    public class Cell : IEquatable<Cell>
    {
        /// <summary>
        /// Значение ячейки.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Преобразует ячейку в строку
        /// </summary>
        /// <returns>Возвращает строковое представление ячейки.</returns>
        public override string ToString()
        {
            return Value;
        }

        /// <summary>
        /// Сравнивает текущую ячейку с другой.
        /// </summary>
        /// <param name="other">Другая ячейка.</param>
        /// <returns>Возвращает true, если ячейки равны, иначе false.</returns>
        public bool Equals(Cell other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Value, other.Value);
        }

        /// <summary>
        /// Сравнивает текущий объект с другим.
        /// </summary>
        /// <param name="obj">Другой объект.</param>
        /// <returns>Возвращает true, если объекты равны, иначе false.</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Cell) obj);
        }

        /// <summary>
        /// Вычисляет хэш.
        /// </summary>
        /// <returns>Возвращает хэш.</returns>
        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }
    }
}