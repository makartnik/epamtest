﻿namespace CSV
{
    /// <summary>
    /// Тип колонки.
    /// </summary>
    public enum ColumnType
    {
        /// <summary>
        /// Строка
        /// </summary>
        String,
        /// <summary>
        /// Дата.
        /// </summary>
        Date,
        /// <summary>
        /// Целое число.
        /// </summary>
        Integer,
        /// <summary>
        /// Число с плавающей точкой.
        /// </summary>
        Float
    }
}