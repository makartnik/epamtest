﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSV
{
    /// <summary>
    /// Парсер CSV-формата.
    /// </summary>
    public class Parser
    {
        /// <summary>
        /// Распознает заголовок.
        /// </summary>
        /// <param name="line">Строковое представление заголовка.</param>
        /// <returns>Возвращает распознанный заголовок.</returns>
        public IEnumerable<Column> ParseHeadline(string line)
        {
            line = ' ' + line;
            var columns = line.Split(';');
            foreach (var column in columns)
            {
                var nameAndType = column.Split(' ')
                    .Where(c => c.Length > 0)
                    .ToArray();
                if (column.Length == 0
                    || column[0] != ' '
                    || nameAndType.Length != 2
                    || nameAndType[0].Length + nameAndType[1].Length + 2 != column.Length)
                    throw new FormatException("Некорректная строка.");


                string name = nameAndType[0];
                ColumnType type;
                try
                {
                    type = (ColumnType) Enum.Parse(typeof(ColumnType), nameAndType[1]);
                }
                catch (Exception)
                {
                    throw new FormatException("Некорректная строка.");
                }
                yield return new Column
                {
                    Name = name,
                    Type = type
                };
            }
        }

        /// <summary>
        /// Распознает строку таблицы.
        /// </summary>
        /// <param name="line">Строковое представление  строки таблицы.</param>
        /// <returns>Возвращает распознанную строку таблицы.</returns>
        public IEnumerable<Cell> ParseRow(string line)
        {
            var parsedRow = new List<Cell>();
            bool required = false;
            var builder = new StringBuilder();
            for (int i = 0; i < line.Length; ++i)
            {
                char current = line[i];
                if (required)
                {
                    builder.Append(current);
                    if (current == '"')
                    {
                        if (i != line.Length - 1 && line[i + 1] == '"')
                        {
                            builder.Append('"');
                            ++i;
                        }
                        else
                        {
                            if (i != line.Length - 1 && line[i + 1] != ';')
                                throw new FormatException("Некорректная строка.");
                            required = false;
                        }
                    }
                }
                else
                {
                    if (current == ';')
                    {
                        if (i == line.Length - 1 || line[i + 1] != ' ')
                            throw new FormatException("Некорректная строка.");
                        ++i;
                        parsedRow.Add(new Cell {Value = builder.ToString()});
                        builder.Clear();
                        continue;
                    }
                    builder.Append(current);
                    if (current == '"')
                    {
                        required = true;
                    }
                }
            }
            if (required)
                throw new FormatException("Некорректная строка.");
            parsedRow.Add(new Cell {Value = builder.ToString()});
            return parsedRow;
        }
    }
}