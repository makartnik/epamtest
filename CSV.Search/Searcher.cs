﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CSV.Search
{
    /// <summary>
    /// Поиск по CSV-файлу.
    /// </summary>
    public class Searcher
    {
        /// <summary>
        /// Поиск по CSV-файлу.
        /// </summary>
        /// <param name="inPath">Путь входного файла.</param>
        /// <param name="outPath">Путь выходного файла.</param>
        /// <param name="encoding">Кодировка файлов.</param>
        /// <param name="columnName">Имя колонки для поиска.</param>
        /// <param name="cell">Ячейка для поиска.</param>
        public void Search(string inPath, string outPath, Encoding encoding, string columnName, Cell cell)
        {
            try
            {
                using (var reader = new Reader(inPath, encoding))
                {
                    var headline = reader.Headline.ToList();
                    var column = headline.Single(c => c.Name == columnName);
                    int columnIndex = headline.IndexOf(column);
                    var checker = new Checker();
                    if (!checker.Check(cell, column.Type))
                        throw new FormatException("Некорректное значение поиска.");
                    using (var writer = new Writer(outPath, encoding, headline))
                    {
                        IEnumerable<Cell> row;
                        while ((row = reader.ReadRow()) != null)
                        {
                            if (row.ElementAt(columnIndex).Equals(cell))
                                writer.WriteRow(row);
                        }
                    }
                }
            }
            catch (FileNotFoundException)
            {
                throw new FileNotFoundException("Не найден файл.");
            }
            catch (UnauthorizedAccessException)
            {
                throw new UnauthorizedAccessException("Отказано в доступе.");
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException("Отсутвует столбец.");
            }
        }
    }
}