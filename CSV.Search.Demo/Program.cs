﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSV.Search.Demo
{
    class Program
    {
        /// <summary>
        /// Точка входа.
        /// </summary>
        /// <param name="args">Аргументы командной строки.</param>
        static void Main(string[] args)
        {
            try
            {
                var options = GetOptions(args);
                var searcher = new Searcher();
                searcher.Search(options.Input, options.Output, options.Encoding,
                    options.Column, new Cell {Value = options.Expression});
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Получает параметры приложения из аргументов командной строки.
        /// </summary>
        /// <param name="args">Аргументы командной строки.</param>
        /// <returns>Возвращает параметрыт приложение.</returns>
        private static Options GetOptions(string[] args)
        {
            if (args.Length != 10)
                throw new ArgumentException("Неверное количество параметров.", nameof(args));
            var arguments = new Dictionary<string, string>();
            for (int i = 0; i < args.Length; i += 2)
                arguments.Add(args[i], args[i + 1]);
            try
            {
                var options = new Options
                {
                    Input = arguments["-in"],
                    Output = arguments["-out"],
                    Column = arguments["-col"],
                    Encoding = Encoding.GetEncoding(arguments["-enc"]),
                    Expression = arguments["-exp"]
                };
                return options;
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentException("Указаны не все параметры.");
            }
        }
    }
}