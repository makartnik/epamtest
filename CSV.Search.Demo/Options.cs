﻿using System.Text;

namespace CSV.Search.Demo
{
    /// <summary>
    /// Параметры приложения.
    /// </summary>
    public class Options
    {
        /// <summary>
        /// Путь входного файла.
        /// </summary>
        public string Input { get; set; }

        /// <summary>
        /// Путь выходного файла.
        /// </summary>
        public string Output { get; set; }

        /// <summary>
        /// Кодировка файлов.
        /// </summary>
        public Encoding Encoding { get; set; }

        /// <summary>
        /// Имя колонки для поиска.
        /// </summary>
        public string Column { get; set; }

        /// <summary>
        /// Выражение для поиска.
        /// </summary>
        public string Expression { get; set; }
    }
}